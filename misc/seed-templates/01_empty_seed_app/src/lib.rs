use seed::{prelude::*, *}; 
use seed::virtual_dom::update_el::UpdateEl;
use seed_icons::fa::regular::check_circle;

// Model describes application state

struct Model {}

impl Default for Model {
    fn default() -> Self { 
      Model {}
    }
}

// view function to render app

fn view(model: &Model) -> Node<Msg> {
    div![]
}

// Msg to pass events happened in app

pub enum Msg {}

// update function to react on events

fn update(_msg: Msg, _model: &mut Model, _: &mut impl Orders<Msg>) {
}

// init function initalizes app

fn init(_: Url, _: &mut impl Orders<Msg>) -> Model {
    Model::default()
}

// And some boilerplate to bootstrap the application

#[wasm_bindgen(start)]
pub fn start() {
    App::start("app", init, update, view);
}
