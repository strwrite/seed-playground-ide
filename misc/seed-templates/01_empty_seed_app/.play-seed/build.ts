// Following is some Play Seed specific boilerplate, 
// you will not need it in production, it exists here only 
// for demo purposes

import * as gulp from "gulp";
import { Service, project } from "@wasm/studio-utils";

gulp.task("build", async () => {
  const options = { seed: true };
  
  // backend would only use content in "src" folder
  const sources = project.list().filter((path: string)  => path.startsWith("src")).map((it: string) => project.getFile(it));
  const data = await Service.compileFiles(sources, "rust", "wasm", options);

  // wasm-bindgen returns js file
  const outJs = project.newFile("out/index.js", "js", true);
  outJs.setData(data["wasm_bindgen.js"]);

  // wasm-bindgen returns wasm file
  const outWasm = project.newFile("out/main.wasm", "wasm", true);
  outWasm.setData(data["a.wasm"]);
});

gulp.task("default", ["build"], async () => {});
