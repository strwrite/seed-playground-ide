use seed::{prelude::*, *}; 
use seed::virtual_dom::update_el::UpdateEl;
use seed_icons::fa::regular::check_circle;

// Model describes application state

struct Model {
    greeting: String,
}

impl Default for Model {
    fn default() -> Self { 
      Model { 
        greeting: "Hello, this is your Seed app!".to_string() 
      }
    }
}

// view function simply renders one div with some styling

fn view(model: &Model) -> Node<Msg> {
    greet_view(&model.greeting)
}

fn greet_view(greeting: &String) -> Node<Msg> { 
    div![
        style!{
            "background-color" => "black", 
            "color" => "white", 
            "min-height" => "5rem",
            "line-height" => "5rem",
            "text-align" => "center", 
        },
        green_check(), 
        greeting
    ] 
}

fn green_check<T>() -> Node<T> {
    span![ 
        style!{"color" => "green", "margin" => "1rem"},
        check_circle::i()
    ]
}

// Msg and update are not used in this example, as it's not interactive

pub enum Msg {}

fn update(_msg: Msg, _model: &mut Model, _: &mut impl Orders<Msg>) {
}

// And some boilerplate to bootstrap the application

#[wasm_bindgen(start)]
pub fn start() {
    App::start("app", init, update, view);
}

fn init(_: Url, _: &mut impl Orders<Msg>) -> Model {
    Model::default()
}
