# Basic Hello Seed App

This is a demo with basic hello application, which only outputs one line with greeting using [Seed](https://seed-rs.org) to render the page.

All the magic happens in `src/lib.rs` file, all the rest here is 
a boilerplate needed for this IDE bootstrap the project. 

Note that only `src` folder would be mounted in the build container.

# src/lib.rs

This file contains application `Model`, which in this example is 
a simple structure with one String in a field `greeting`, 
`update` function and `Msg` are not used, and `view` function
renders one `div` element with some additional styling.

# main.html

Main entry point of frontend application. In this demo it invokes
main.js, which is Play Seed specific. When you're using Seed in production,
you'll probably have slightly different code there. Refer to 
[Seed](https://seed-rs.org) documentation for details on that.

# main.js

This file is not related to Seed itself, it's a part of Play Seed IDE 
project bootstrap.
It includes some boilerplate needed to run the demo in this environment.

# build.ts

This file is not related to Seed itself, it's a part of Play Seed IDE 
project bootstrap. 
It includes the logic, which is executed in IDE, when you run the build.

# package.json

This file is not related to Seed itself, it's a part of Play Seed IDE 
project bootstrap.
It includes description of this example project.
